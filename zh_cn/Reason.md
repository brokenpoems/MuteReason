- 正常情况:

1. IncorrectLanguage 在其他频道使用非该频道的语言(视情况而定当然有可能是刚好要用)
2. Spam 刷屏或其他垃圾消息
3. BadMeme 玩烂梗
4. Swearing 粗话
5. NSFW Not Safe for Work 搞颜色
6. BadName 用户名违规(例如有骂人等情况)之前是直接2147483647 现在是84 然后直接强制改名
7. Translator 翻译机
8. IgnoringWarning 忽视警告
9. KY 读不懂空气(空気が読めない)
10. Aggressive 过激言论或者语言中带有挑衅
11. Provocation 挑衅管理(你**有本事就禁言我啊)
12. Politics 政治话题
13. SensitiveTopics 敏感话题

- 特殊情况:
1. Racist 种族歧视 直接2147483647
2. Wants2BeMuted 嗯 你要试试的 看你要的时间

正常情况禁言时间:
第一次一般为 80-90小时(处理的人不同时间也不同)
(我的是84小时)
禁言时间随次数翻倍,以及情况数也会加上
(例如又刷屏又玩烂梗)
一般第4或第5次就会被2147483647
(待补充)